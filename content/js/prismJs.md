{{title:prism.jsをつかう}}

###このごろコードのシンタックスはprism.js

以下のページでDLします。

<a href="http://prismjs.com/" target="_blanck">prism.js</a>

<br>
--------------

###使い方

ページからDLしてきたJSファイルとCSSファイルを
読み込む

```
<link href="vendors/prism/prism.css" rel="stylesheet">
<script src="vendors/prism/prism.js"></script>
```

htmlでコードをかっこよく見せたいところに

```
<pre><code class="language-<コードの言語>">コードを書く
</code></pre>
```

以上。

**超簡単!!!!!!!!!!!!!!!!!**

###プラグイン

* Line Numbers:行番号を表示
* Show Language	：マウスオーバー時に言語を表示
* Toolbar：Copy to Clipboard Buttonの使用に必要
* Copy to Clipboard Button：コピーボタンをつける

Line Numbersの使用はhtmlをちょっといじらんとだめです。

```
<pre class="line-numbers"><code class="language-<コードの言語>">コードを書く
</code></pre>
```
とする