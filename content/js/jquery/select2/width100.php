{{title:select2のデザインをwidth100%にする}}



<div class="block">
	<div class="block_inner">
		<ul>
			<li>
				<p>5161行目付近の</p>
				<p>
					<code>return 'auto';</code>と<code>return elementWidth + 'px';</code>を<br>
					<code>return '100%';</code>で返したらOK！
				</p>
			</li>
			<li>
				<pre><code class="js">if (method == 'element') {
	var elementWidth = $element.outerWidth(false);
	if (elementWidth <= 0) {
		// return 'auto';
		return '100%';
	}
	// return elementWidth + 'px';
	return '100%';
}
				</code></pre>
			</li>
		</ul>
	</div>
</div>