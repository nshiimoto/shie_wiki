{{title:Wordpressの移動のしかた}}


##始めに

今まで、domein.com/test/で開発していたWordpressを  
ドメイン直下(domein.com/)にアクセスしたら、  
wodpressが走るようにする手順を解説します。  
**違うサーバーへの引っ越しも手順は一緒ですので、読みかえて作業してください。**


----------

##移動前のディレクトリ構造

以下のようなディレクトリ構造にWordpressを配置し,  
ブラウザでアクセスしたければ、  
 * 公開側：`http://domein.com/test`
 * 管理側：`http://domein.com/test/wp/wp-admin/`


	documentRoot/
		test/	(③　wordpress本体が入っているフォルダ)
			wp-admin/
				..(いっぱいファイルとかフォルダとかあるけど、書かない　基本、フォルダの中にはいっぱい何かがあると思っといてください)
			wp-content/
				languages/
				plugins/	(プラグインが入るフォルダ)
				themes/		(見た目のテーマが入るフォルダ)
				upgrade/
				uploads/	(管理画面でアップロードした画像が入るフォルダ)
				index.php
			wp-includes/
			.htaccess	(①　wordpressへのアクセスを制御する.htaccess)
			index.php	(②　wordpress全体のindex.php)
			.htaccess
			license.txt
			readme-ja.html
			readme.html
			wp-activate.php
			wp-blog-header.php
			wp-config-sample.php
			wp-config.php		(④　接続DBの情報があるファイル)
			wp-cron.php
			wp-links-opml.php
			wp-load.php
			wp-login.php
			wp-mail.php
			wp-settings.php
			wp-signup.php
			wp-trackback.php
			xmlrpc.php

※`test/`はtestフォルダという事

---

##移動後のディレクトリ構造

以下のような形で本番運用したい
 * 公開側：`http://domein.com/`
 * 管理側：`http://domein.com/wp/wp-admin/`


	documentRoot/
		.htaccess	(①wordpressへのアクセスを制御する.htaccess)
		index.php	(②wordpress全体のindex.php)
		wp/		(wordpress本体が入っているフォルダ)
			wp-admin/
			wp-content/
				languages/
				plugins/	(プラグインが入るフォルダ)
				themes/		(見た目のテーマが入るフォルダ)
				upgrade/
				uploads/	(管理画面でアップロードした画像が入るフォルダ)
				index.php
			wp-includes/
			.htaccess
			license.txt
			readme-ja.html
			readme.html
			wp-activate.php
			wp-blog-header.php
			wp-config-sample.php
			wp-config.php		(③接続DBの情報があるファイル)
			wp-cron.php
			wp-links-opml.php
			wp-load.php
			wp-login.php
			wp-mail.php
			wp-settings.php
			wp-signup.php
			wp-trackback.php
			xmlrpc.php

-----

##具体的な移動するフォルダと移動先と修正するフォルダ

<div class="row">
	<div class="col-xs-10">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>手順No</th>
					<th>移動元ファイル名パス</th>
					<th>移動先ファイル名パス</th>
				</tr>
			</thead>
			<tboody>
				<tr>
					<td>1</td>
					<td>documentRoot/test/</td>
					<td>documentRoot/wp/</td>
					<td>
						testフォルダをwpフォルダに名前を変える<br>
						wordpressの本体が入ったフォルダを移動したい先のフォルダに移動する。
					</td>
				</tr>
				<tr>
					<td>2</td>
					<td>documentRoot/wp/.htaccess</td>
					<td>documentRoot/.htaccess</td>
					<td>1の手順でwpに名前を変えたので<br>wpというフォルダ名を使用しています。</td>
				</tr>
				<tr>
					<td>3</td>
					<td>documentRoot/twp/index.php</td>
					<td>documentRoot/index.php</td>
					<td></td>
				</tr>
				<tr>
					<td>4</td>
					<td>documentRoot/wp/wp-config.php</td>
					<td> `移動させずにファイルの修正のみ` </td>
					<td></td>
				</tr>
			</tboody>
		</table>
	</div>
</div>

--------

##実際に作業する

####手順1　フォルダ名を変える

フォルダ名を変更しよう

<br>

####手順2　.htaccessを移動する。

名前を変えた`documentRoot/wp/`フォルダ内にある  
`.htaccess`ファイルを  
`documentRoot/`フォルダに移動する。  
**要は、wpフォルダか出して一個上の階層のフォルダに移動する。**

移動した`.htaccess`ファイルを開いて修正する

**修正前**

	# BEGIN WordPress
	<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteBase /test/
	RewriteRule ^index\.php$ - [L]
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule . /test/index.php [L]
	</IfModule>

**修正後**

	# BEGIN WordPress
	<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteBase /		//　※index.phpがあるフォルダを指定する。
	RewriteRule ^index\.php$ - [L]
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule . /index.php [L]　//　※index.phpがあるフォルダを指定する。
	</IfModule>

<br>

####手順3　.htaccessを移動する。

名前を変えた`documentRoot/wp/`フォルダ内にある  
`index.php`ファイルを  
`documentRoot/`フォルダに移動する。  
**要は、wpフォルダか出して一個上の階層のフォルダに移動する。**

移動した`index.php`ファイルを開いて修正する

17行目を変更

```php
require( dirname( __FILE__ ) . '/wp-blog-header.php' );
```
↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

```php
require( dirname( __FILE__ ) . '/wp/wp-blog-header.php' );
```

移動前は同じフォルダにあった`index.php`と`wp-blog-header.php`を  
wpフォルダの中の`wp-blog-header.php`を  
ドキュメントルートの`index.php`から指定する。

<br>

####手順4　wp-config.phpを編集する

23行名以降

```php
/** WordPress のためのデータベース名 */
define('DB_NAME', '本番用のDB名にする');

/** MySQL データベースのユーザー名 */
define('DB_USER', '本番用のユーザー名にする');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', '本番用のパスワードにする');

/** MySQL のホスト名 */
define('DB_HOST', '本番用のDBURLにする');
```

DBのホスト名・DB名・ユーザー名・パスワードは移動先の物を使用します。
<br>


-----

##DBの登録値を本番DBに移動する。

<div class="row">
	<div class="col-xs-10">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>手順No</th>
					<th>対象テーブル</th>
					<th>作業内容</th>
				</tr>
			</thead>
			<tboody>
				<tr>
					<td>5</td>
					<td>全テーブル</td>
					<td>テスト環境からDBを全てエクスポートする。</td>
					<td></td>
				</tr>
				<tr>
					<td>6</td>
					<td>全テーブル</td>
					<td>エクスポートしたSQLファイルの中のテスト環境URLを本番環境URLに変更</td>
					<td></td>
				</tr>
				<tr>
					<td>7</td>
					<td>全テーブル</td>
					<td>本番環境DBにインポート</td>
					<td></td>
				</tr>
				<tr>
					<td>8</td>
					<td>wp_options</td>
					<td>
						option_name = "siteurl"　のoption_valueを本番環境の移動するフォォルダ名までのURLにする。<br>
						option_name = "home"　のoption_valueを本番環境のトップページのURLにする。<br>
					</td>
					<td></td>
				</tr>
			</tboody>
		</table>
	</div>
</div>

####手順5　テスト環境のDBをdump(エクスポート)して本番環境にインポートする。

[DBからエクスポートはここ]({rootUrl})にある。
[新たにDBを作ってインポートはここ]({rootUrl})にある。



---------------------------
		⑤ファイルのパーミッションを変更
---------------------------
	/
		wp-config.php		404
		.htaccess			604
		wp-content			757
			配下も			757
		その他のフォルダ	755
			配下も			755
		その他のファイル	755

でいいはず。

---------------------------
		⑥前ページリンク切れチェック
---------------------------
wordpressはDBの値に張り切って
ドメインからのURLやsrcやherfを書いていたりするので
全ページ。全リンクをチェックする事
その時にローカルのxampはストップさせておくこと。

椎本の失敗談ですが。
	一回動いてるわ～と思ったら、
	いつの間にかURLがhttp://localhostに変わってた事が有ります。
	んで本番環境は全滅でした。
	めっちゃクライアントにあやまりました。
