{{title:sublimeに入れたプラグイン}}

###パッケージコントローラーのインストール方法

[パッケージコントロールのインストール](http://futago-life.com/sublime-text3-wiki/first-step/install/package-control)

<br>

---------------

###インストール済みパッケージ

|      カテゴリ      |     パッケージ名    |                   説明                   |
|--------------------|---------------------|------------------------------------------|
| 日本語             | Japanize            | 日本語化                                 |
|                    | IMESupport          | 日本語入力に対応させる                   |
|                    | convertToUTF8       | UTF8以外のファイルでもとりあえず開かせる |
| 便利系             | SFTP                | FTPに対応させる                          |
|                    | bracketHighligher   | ソースのブロックをわかりやすく           |
|                    | SideBarEnhancements | サイドバーに機能を増やす                 |
|                    | AutoFileName        | ファイル名を補完                         |
| コード全般         | Emmet               | html自動補完                             |
|                    | DocBlockr           | phpDocなどの補完                         |
| マークダウン       | Markdown Extended   | マークダウンのシンタックス               |
|                    | Table Editor        | markdown時のtableの自動補完              |
| 補完とシンタックス | SCSS                | scssのシンタックスと補完                 |
|                    | Phpcs               | phpの補完                                |


<br>

---------------

###入れようと思うパッケージ

|     カテゴリ     |       パッケージ名      | 説明 |
|------------------|-------------------------|------|
| ソース楽にする系 | CodeFormatter           |      |
|                  | CSS Snippets            |      |
|                  | jQuery                  |      |
|                  | SublimeLinter           |      |
|                  | SublimeLinter-csslint   |      |
|                  | SublimeLinter-html-tidy |      |
|                  | SublimeLinter-jshint    |      |


<br>

---------------

###テーマ

Material Theme

<br>

---------------

###フォント

[Migu 1M](http://mix-mplus-ipa.osdn.jp/migu/)

<br>

---------------

###設定

```json
{
	"color_scheme": "Packages/Material Theme/schemes/Material-Theme.tmTheme",
	"draw_white_space": "all",
	"font_face": "Migu 1M",
	"font_size": 12,
	"highlight_line": true,
	"ignored_packages":
	[
		"Vintage"
	],
	"show_encoding": true,
	"tab_size": 2,
	"theme": "Material-Theme.sublime-theme",
	"translate_tabs_to_spaces": false
}
```


