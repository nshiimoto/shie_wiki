{{title:cebe/markdownのカスタム}}

###cebe/markdownをcomposerでインストールする

最新バージョンはgithubで確認する  
<a href="https://github.com/cebe/markdown" target="_blank">cebe/markdown</a>


#####①プロジェクトルートに`composer.json`ファイルを作成  
```json
{
	"require": {
		"cebe/markdown": "v1.1.1"
	}
}
```
#####②コマンドラインを起動してプロジェクトルートでcomposerコマンド実行
```
cd C:\xampp\htdocs\sysroot
composer install
```

あとはお好きなように

<br>
-----------

###cebe/markdownのカスタムをする

`<pre>`タグにプログラム言語のクラスを入れるようにする  
言語の指定がなければ黙ってhtmlにするようにする。

cube/markdownはcomposerでインストールしたもを使用しているので、  
以下のカスタムは間違いなく邪道というかプログラマーとしてどうかと思うwww　


```php
//sysroot/vendor/cebe/markdown/block/FencedCodeTrait.php
//49行目
//if (!empty($language)) {
//	$block['language'] = $language;
//}
if (!empty($language)) {
	$block['language'] = $language;
}else{
	$block['language'] = 'html';
}
```

```php
//sysroot/vendor/cebe/markdown/block/CodeTrait.php
//61行目付近
//return "<pre><code$class>" . htmlspecialchars($block['content'] . "\n", ENT_NOQUOTES | ENT_SUBSTITUTE, 'UTF-8') . "</code></pre>\n";
return "<pre class=\"line-numbers\"><code$class>" . htmlspecialchars($block['content'] . "\n", ENT_NOQUOTES | ENT_SUBSTITUTE, 'UTF-8') . "</code></pre>\n";
```
これでOK！