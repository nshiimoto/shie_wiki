{{title:SCSSを使う}}

<div class="block">
	<h3>目次</h3>
	<ol class="mokuji">
		<li><small>STEP.1</small>　rubyをインストール</li>
		<li><small>STEP.2</small>　gemをupdate</li>
		<li><small>STEP.3</small>　gemでsassとcompassをインストール</li>
		<li>
			<a href="#step4">
				<small>STEP.4</small>　config.rbをプロジェクトのルートに作成
			</a>
		</li>
		<li>
			<a href="#step5">
				<small>STEP.5</small>　compass_start.batをプロジェクトルートに作成
			</a>
		</li>
		<li>
			<a href="#step6">
				<small>STEP.6</small>　SCSSを保存するたびに自動でcssファイルを作成する。
			</a>
		</li>
	</ol>
</div>

<!-- ここから先が具体的なやり方 -->
<div id="step4" class="block">
	<h3><small>STEP.4</small>　config.rbをプロジェクトのルートに作成</h3>
	<div class="block_inner">
		<ul>
			<li>
				<p>以下のサイトを参考にして作成する。</p>
				<p>
					<a href="http://rfs.jp/sb/css/sass/sass10_config.html" target="_brank">http://rfs.jp/sb/css/sass/sass10_config.html</a>
				</p>
			</li>
		</ul>
		<ol>
			<li>
				プロジェクトルートに<code>config.rb</code>ファイルを作成する。
			</li>
			<li>
				ファイルの中身は以下のようにする。<br>
				<pre><code># config.rbからみたプロジェクトルートのパス
http_path = "/"
# プロジェクトルートから見た、各フォルダのパス
css_dir = "theme/css"
sass_dir = "theme/scss"
images_dir = "theme/img"
javascripts_dir = "theme/js"

#作成されるCSSファイルの一番上に@charset "UTF-8";が入る設定
Encoding.default_external = 'utf-8'

# ######################################
# 以下は本番リリース時のみ外す
# ######################################
#CSS出力スタイル １セレクタ１行で書くスタイル
# output_style = :compact

#出力されるCSSにSCSSの行番号を表示するか？
# line_comments = false</code></pre>
としておく
				</li>
			</ol>
		</div>
	</div>
	<div id="step5" class="block">
		<h3><small>STEP.5</small>　compass_start.batをプロじゅえくとルートに作成</h3>
		<div class="block_inner">
			<ol>
				<li>
					プロジェクトルートに<code>compass_start.bat</code>という名前でファイルを作成する。
				</li>
				<li>
					ファイルの中身は以下のようにする。<br>
					<pre><code>compass watch</code></pre>
					たったこれだけでOK！
				</li>
			</ol>
		</div>
	</div>
	<div id="step6" class="block">
		<h3><small>STEP.6</small>　SCSSを保存するたびに自動でcssファイルを作成する。</h3>
		<div class="block_inner">
			<ol>
				<li>
					プロジェクトルートの<code>compass_start.bat</code>をダブルクリックして、コマンドプロンプトを開く<br>
					<br>
					<img src="[rooturl]/web/img/scss/index/step6_img1.jpg" alt="">
				</li>
				<li>
					1.のコマンドプロンプト画面が表示されたら、<br>
					<code>config.rb</code>で設定したcsccディレクトリ上の<br>
					csccファイルを編集して保存すると自動的にそのファイルだけCSSを出力してくれる<br>
					<br>
					<img src="[rooturl]/web/img/scss/index/step6_img2.jpg" alt=""><br>
					上記画面は<code>custom.scss</code>を編集して保存しので、<br>
					<code>custom.css</code>を自動で出力してくれた状態
				</li>
			</ol>
		</div>
	</div>