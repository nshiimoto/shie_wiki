
###初めに

-------------------

**前提条件**  
○ドメインは[domein]とする。  
○wordpressのファイルは「wp」フォルダに入れることにする。(以降、[wordpressDir]とする。)

<br>
<br>

---------------
###①「　[domein]　/　[wordpressDir]　/index.php」を「　[domein]　/index.php」に移動
<br>

---------------
###②「index.php」を修正
　[domein]　/index.phpの  
17行目を変更  

	require( dirname( __FILE__ ) . '/wp-blog-header.php' );

を

	require( dirname( __FILE__ ) . '/wp/wp-blog-header.php' );

<br>

--------------------
###②DBの接続情報を本番環境に合わせる

　[wordpressDir]　/wp-config.phpの23行目から39行目を修正

	/** WordPress のためのデータベース名 */
	define('DB_NAME', 'データベース名');
	
	/** MySQL データベースのユーザー名 */
	define('DB_USER', 'データベースユーザー名');
	
	/** MySQL データベースのパスワード */
	define('DB_PASSWORD', 'パスワード);
	
	/** MySQL のホスト名 */
	define('DB_HOST', 'データベースURL');
	
	/** データベースのテーブルを作成する際のデータベースの文字セット */
	define('DB_CHARSET', 'utf8');
	
	/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
	define('DB_COLLATE', '');

<br>

--------------------
###③DB無いのURL情報を本番にそくした情報に変換する。

wp_options テーブルの「siteurl」と「home」のoption_valueを変える。  

|タイトル|値|説明|
|:------|:------|:------|
|**siteUrl**|http://　[domein]　/　[wordpressDir]　/|本体が有るURL|
|**home**|http://　[domein]/　|ブラウザでトップページが表示されるURL|


<br>

-------------------------
###④「　[domein]　/　[wordpressDir]　/.htaccess」を「　[domein]　/.htaccess」に移動

<br>

-------------------------
###⑤「　[domein]　/.htaccess」を修正

**修正前**

	# BEGIN WordPress
	<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteBase /テスト環境ディレクト名/
	RewriteRule ^index\.php$ - [L]
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule . /[テスト環境ディレクト名]/index.php [L]
	</IfModule>

**それを下記のように変更する。**

	# BEGIN WordPress
	<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteBase /
	RewriteRule ^index\.php$ - [L]
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule . /index.php [L]
	</IfModule>

　※ドメイン直下ではなく「wp」ディレクトリの中でwordpressを実行する時

ようは、

	RewriteBase /テスト環境ディレクト名/
↓↓↓

	RewriteBase /

と

	RewriteRule . /mizuden_wp/index.php [L]

↓↓↓

	RewriteRule . /index.php [L]

とする。  
直下で動作させるときは  
　　※○/wp/の部分を/に変更すればいい。

<br>

---------------------------
###⑤ファイルのパーミッションを変更

|ファイルやフォルダ名|パーミッション|
|:-------|:-------|
|wp-config.php|404|
|.htaccess|604|
|wp-content(フォルダ)|757|
|その他のフォルダ|755|
|その他のフォルダの配下もすべて|755|
|その他のファイル|755|

でいいはず。

<br>

---------------------------
###⑥前ページリンク切れチェック

wordpressはDBの値に張り切って  
ドメインからのURLやsrcやherfを書いていたりするので  
全ページ。全リンクをチェックする事  

---------------
#####その時にローカルのxampはストップさせておくこと。

--------------------

<br>

####shieの失敗談ですが。
一回動いてるわ～と思ったら、  
いつの間にかURLがhttp://localhostに変わってた事が有ります。  
んで本番環境は全滅でした。  
めっちゃクライアントにあやまりました。
