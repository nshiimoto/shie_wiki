<?php 

function getSideTreeHtml($ary,$rootUrl ,$childFlg = FALSE){
	$html = '';
	foreach($ary as $row){
		$html .=  '<li>'.PHP_EOL;
		$html .= '		<a>'.PHP_EOL;
		if($childFlg === FALSE){
			$html .=  '		<i class="fa fa-file-text-o"></i>';
		}
		$html .=  '			'.$row->title.'<span class="fa fa-chevron-down"></span>'.PHP_EOL;
		$html .= '		</a>'.PHP_EOL;
		$html .=  '		<ul class="nav child_menu">'.PHP_EOL;
		foreach($row->page as $page){
			$html .=  '			<li><a href="'.$rootUrl.$page->uri.'">'.$page->title.'</a></li>'.PHP_EOL;
		}
		if(isset($row->child)){
			$html .= getSideTreeHtml($row->child,$rootUrl,TRUE);
		}
		$html .=  '		</ul>';
		$html .=  '</li>';
	}
	return $html;
}

?>