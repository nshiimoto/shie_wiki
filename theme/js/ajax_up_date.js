// jQuery.noConflict();
(function($) {

	ajaxPageUpdate();
	ajaxDirUpdate();

	function ajaxDirUpdate(){
		$.ajax({
			type: "POST",
			url: rootUrl+'page/ajaxDirSave',
			data: "{}",
			success: function(msg){

			},
			error:function(msg){
				alert('Dirシステムエラー');
			}
		});
	}
	
	function ajaxPageUpdate(){
		$.ajax({
			type: "POST",
			url: rootUrl+'page/ajaxPageSave',
			data: "{}",
			success: function(msg){

			},
			error:function(msg){
				alert('Pageシステムエラー');
			}
		});
	}

})(jQuery);