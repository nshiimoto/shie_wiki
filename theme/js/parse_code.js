// jQuery.noConflict();
(function($) {

	parse_code();



	function parse_code(){
		codeList = {
			'"':'&quot;',
			'<':'&lt;	',
			'>':'&gt;',
			'¥':'&yen;'
		}
		$('code').each(function(){
			var elm = $(this);
			var str = elm.html();
			for(key in codeList){
				str = replaceAll(str,key,codeList[key]);
			}
			elm.html(str);
			console.log(str);
		})
	}

	function replaceAll(str, before, after){
		return str.split(before).join(after);  // "a-b-c"
	}

})(jQuery);