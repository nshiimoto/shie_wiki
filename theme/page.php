<?php /*Gentelella Alela! */ ?>
<?php include_once(dirname(__FILE__).'/function.php'); ?>
<!DOCTYPE html>
<html lang="jp">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{title}|shie_Wiki </title>

	<!-- Bootstrap -->
	<link href="{viewUrl}vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="{viewUrl}vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- Prism-->
	<link href="{viewUrl}vendors/prism/prism.css" rel="stylesheet">
	<!-- Custom Theme Style -->
	<link href="{viewUrl}vendors/custom.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/earlyaccess/mplus1p.css" rel="stylesheet" />
	<!-- jQuery -->
	<script src="{viewUrl}vendors/jquery/jquery.min.js"></script>
</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<!-- システムロゴ -->
					<div class="navbar nav_title" style="border: 0;">
						<a href="{rootUrl}" class="site_title"><i class="fa fa-paw"></i> <span>shie_Wiki</span></a>
					</div>
					<div class="clearfix"></div>
					<br />
					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<ul class="nav side-menu">
								<?php /*トップページのURL*/ ?>
								<li><a href="{rootUrl}"><i class="fa fa-home"></i>HOME</a></li>
								<?php /*表層ページのtree*/ ?>
								<?php print getSideTreeHtml($sideTree,$rootUrl); ?>
							</ul>
						</div>
					</div>
					<!-- /sidebar menu -->

					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="FullScreen">
							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Lock">
							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
							<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
						</a>
					</div>
					<!-- /menu footer buttons -->
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">

							<?php /*
							<li><a href="">ログイン</a></li>
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									ユーザー名
									<span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<li><a href="javascript:;"> ログイン情報変更</a></li>
									<li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
								</ul>
							</li>
							*/ ?>
							<li class="top_nav_search">
								<div class="top_search">
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Search for...">
										<span class="input-group-btn">
											<button class="btn btn-default" type="button">Go!</button>
										</span>
									</div>
								</div>
							</li>
							<li><a>検索</a></li>

						</ul>
					</nav>
				</div>
			</div>
			<div class="clearfix"></div>
			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h1>{title}</h1>
						</div>
					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-xs-12">
							<div class="x_panel">
								<div class="x_content">
									{content}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /page content -->
			<script>
				rootUrl = '{rootUrl}';
			</script>
			<script src="{viewUrl}js/ajax_up_date.js"></script>
			<!-- <script src="{viewUrl}js/parse_code.js"></script> -->
			<!-- footer content -->
			<footer>
				<div class="pull-right">
					Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
				</div>
				<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
		</div>
	</div>


	<!-- Bootstrap -->
	<script src="{viewUrl}vendors/bootstrap/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<!-- <script src="{viewUrl}vendors/fastclick/fastclick.js"></script> -->
	<!-- Custom Theme Scripts -->
	<!-- <script src="{viewUrl}vendors/custom.min.js"></script> -->
	<script src="{viewUrl}vendors/custom.js"></script>
	<script src="{viewUrl}vendors/prism/prism.js"></script>
</body>
</html>
