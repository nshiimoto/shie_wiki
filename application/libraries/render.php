<?php 

class render{
	var $content = '';
	var $title='';
	var $pagePath='';

	/**
	 *<code></code>の中にあるkeyの文字をvalに変換する
	 */
	var $metaStrAry = array(
		'"'=>'&quot;',
		'<'=>'&lt;	',
		'>'=>'&gt;',
		'¥'=>'&yen;',
		'{' => '&#123;'
		);



	public function __construct(){
		$this->Ci =& get_instance();
		$this->Ci->load->library('parser');
		$this->pagePath = $this->Ci->config->item('page_path');
	}


	/**
	 * ページコンテンツをテキストからhtmlに変換する。
	 * @param  [str] $path [テキストファイルのフルパス]
	 * @param  [array] $data [controllerの$this->data]
	 * @return [ary]
	 */
	public function parse($path,$data){
		//コンテンツを取得
		$str = $this->_getContent($path);
		//markdownを変換
		$str = $this->parseMd($str);
		//codeタグの中身を変換
		$str = $this->_escapeMetaStrInCode($str);
		//titleを取得・削除して純粋なコンテンツhtmlに変換
		$str  = $this->_getContentTitle($str);
		//phpを実行
		$str = $this->Ci->parser->parse_string($str, $data,TRUE);
		//tableタグにbootstrap対応のclassをつける
		$this->content = $this->_setBootstrapClassToTableTag($str);
		return array(
			'content' => $this->content,
			'title' =>$this->title,
			);
	}

	/**
	 * マークダウンファイルをhtmlに変換する。
	 * @param  [str] $content [ファイルの中身の文字列]
	 * @return [str]
	 */
	public function parseMd($str){
		$parser = new \cebe\markdown\GithubMarkdown();
		return $parser->parse($str);
	}


	/********************************************************************/
	/*　				以下プライベートメソッド					*/
	/********************************************************************/


	/**
	 * [_getContent description]
	 * @param  [type] $path [description]
	 * @return [type]       [description]
	 */
	private function _getContent($path){
		return file_get_contents($this->pagePath.$path, "r");
	}



	/**
	 * ページの中身から{{title:〇〇〇〇〇}}の〇〇〇〇〇部分を抜き取って
	 * $this→titleに代入する
	 * @param  [str] $str [ファイルの中身の文字列]
	 * @return [str]
	 */
	private function _getContentTitle($str){
		$this->title = '';
		$str = preg_replace_callback(
			'/{{title:(.*)}}/',
			array($this,'_callbackSetTitle'),
			$str
			);
		return $str;
	}
	/**  _getContentTitleのコールバック関数 */
	private function _callbackSetTitle($m){
		$this->title = $m[1];
		return  '';
	}

	/**
	*<code></code>の中のメタ文字を$this→metaStrAryのkeyからvalに変換する
	* @param  [str] $str [ファイルの中身の文字列]
	* @return [str]
	*/
	private function _escapeMetaStrInCode($str){
		$str = preg_replace_callback(
			'/\<code.*?\>(.*?)\<\/code\>/s',
			array($this,'_callbackEscapeMetaStr'),
			$str
			);
		return $str;
	}
	/**  _escapeMetaStrInCodeのコールバック関数 */
	private function _callbackEscapeMetaStr($m){
		$w = str_replace($m[1],'{{targ}}',$m[0]);
		$s = $m[1];
		foreach($this->metaStrAry as $targ => $rep){
			$s = str_replace($targ,$rep,$s);
		}
		$ret = str_replace('{{targ}}',$s,$w);
		return $ret;
	}

	private function _setBootstrapClassToTableTag($str){
		$str = str_replace('<table>','<table class="table table-hover" >',$str);
		return $str;
	}

}

?>