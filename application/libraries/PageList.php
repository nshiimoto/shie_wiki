<?php 
/**
 * ユーティリティー関数回りのクラス
 *
 ******************************************************************
 *
 **************************************************************** 
 */
class PageList{
	//TODO　　getFileListHush関数を毎回動作させないように変数にファイル一覧配列を
	/**
	 * 指定フォルダ内のファイルやデータで指定の拡張子のファイルのみを全部取得する。
	 * @param  [str] $dir [指定フォルダ]
	 * @param  [str] $ext=null [取得するファイルの拡張子]
	 * @return [ary]      [フォルダ内のファイルリスト]
	 */
	public function getFileList($dir,$ext=NULL){
		$fileHush = $this->getFileListHush($dir,$ext);
		$list = array();
		foreach($fileHush as $filePath){
			$list = $this->_pathToAry($filePath,$list);
		}
		return $list;
	}

	public function getFileListHush($dir,$ext=NULL){
		$iterator = $this->_getFileListObj($dir);
		$dir = rtrim($dir,'/');
		$list = array();
		foreach ($iterator as $fileinfo) {
			//ファイルであり（指定拡張子が有り、拡張子が違う）以外の場合のみファイルリストに追加する。
			if ($fileinfo->isFile() && !($ext !== NULL && $ext != $fileinfo->getExtension())) {
				// $fileExt = $fileinfo->getExtension();
				$filePath = $fileinfo->getPathname();
				$filePath = ltrim(str_replace(array($dir,'\\'),array('','/'),$filePath),'/');
				$list[] = $filePath;
			}
		}
		return $list;
	}

	private function _getFileListObj($dir){
		$dir = rtrim($dir,'/');
		$iterator = new RecursiveDirectoryIterator($dir);
		$iterator = new RecursiveIteratorIterator($iterator);
		return $iterator;
	}

	private function _pathToAry($path,$list,$one = NULL){
		$pathAry = explode('/',$path,2);
		if(isset($pathAry[1])){
			if(!(isset($list[$pathAry[0]]))){
				$list[$pathAry[0]] =array();
			}
			$list[$pathAry[0]] =  $this->_pathToAry($pathAry[1],$list[$pathAry[0]] );
		}else{
						//指定フォルダの表層にあるファイル
			$list[] = $pathAry[0];
		}
		array_multisort($list);
		return $list;
	}

	public function getFileInfo($file){
		$ret = array(
			'name' => '',
			'ext' =>'',
			);
		$fileAry = explode('.',$file);
		if(count($fileAry)===1){
			$ret['name'] = $file;
		}else{
			$i = 0;
			foreach($fileAry as $key => $val){
				if(count($fileAry)-1 > $i ){
					if($i > 0){
						$ret['name'] .= '.';
					}
					$ret['name'] .= $val;
				}else{
					$ret['ext'] = $val;
				}
				$i++;
			}
		}
		return $ret;
	}
}

?>