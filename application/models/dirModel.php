<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class dirModel extends MY_Model
{


	public function __construct()
	{
		parent::__construct();
	}


	public function save($pageList){
		$this->db->empty_table('dir');
		$data = array();
		foreach($pageList as $path){
			$info = $this->pagelist->getFileInfo($path);
			$parentDir = $this->_getParentDir($info['name']);
			$fileName = str_replace($parentDir,'',$info['name']);
			if(!isset($data[$parentDir]) or $fileName == 'index'){
				$data[$parentDir] = array(
					'dir_path'=>$parentDir,
					'index_lg' =>  ( $fileName == 'index' ) ? 1 : 0,
					'level' => count(explode('/',$info['name']))-1
					);
			}
			$pAry = explode('/',$parentDir);
			$dir = '';
			for($i = 0; $i< count($pAry);$i++){
				if($pAry[$i] !== ''){
					$dir = $dir.$pAry[$i].'/';
					if(!isset($data[$dir])){
						$data[$dir] = array(
							'dir_path'=>$dir,
							'level' => $i+1,
							'index_lg' =>  0,
							);
					}
				}
			}
		}
		foreach($data as $row){
			$this->db->replace('dir', $row);
		}
	}


	public function getDirTree($level ,$dir = NULL){
		$this->db
		->select(array('dir_path','index_lg'))
		->from('dir')
		->where('level',$level);
		if(!is_null($dir)){
			$this->db->like('dir_path',$dir);
		}
		$q = $this->db->get();
		$ary = array();
		foreach($q->result() as $key => $row){
			$row->title = '';
			if($row->index_lg == 1){
				$row->title = $this->PageModel->getTitleFromUri($row->dir_path.'index');
			}
			if($row->title == ''){
				$row->title = $row->dir_path;
			}
			$row->child = $this->getDirTree($level+1,$row->dir_path);
			$row->page= $this->PageModel->getDirPage($row->dir_path);
			array_push($ary,$row);
		}
		return $ary;
	}
}
