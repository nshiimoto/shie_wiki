<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class pageModel extends MY_Model
{


	public function __construct()
	{
		parent::__construct();
	}

	public function getRow($uri){
		$q = $this->db
		->select('path')
		->select('ext')
		->from('page')
		->where('uri',$uri)
		->get();
		return $q->row();
	}

	/**
	 * pageテーブルに第１引数に配列として渡したパスのデータを保存する。
	* @param  [ary]　array(0=>'pagePath.ext' 1=>'pagePath.ext')
	* @param  [ary]　コントローラー $this->data
	* @return [none]
	*/
	public function save($pageList,$data){
		$this->db->empty_table('page');
		$ary = array();
		foreach($pageList as $path){
			$info = $this->pagelist->getFileInfo($path);
			$content = $this->render->parse($path,$data);
			$row = array(
				'path'=>$path,
				'uri' => $info['name'],
				'parent_dir' => $this->_getParentDir($info['name']),
				'level' => count(explode('/',$info['name']))-1,
				'content' =>str_replace('\'','\\\'',$content['content']),
				'title'=>$content['title'],
				'ext'=>$info['ext']
				);
			$this->db->replace('page', $row);
		}
	}


	public function getDirPage($dir){
		$q = $this->db
		->select(array('uri','title'))
		->from('page')
		->where('parent_dir',$dir)
		->get()->result();
		foreach($q as $key => $row){
			if($row->title == ''){
				$q[$key]->title = $row->uri;
			}
		}
		return $q;
	}

	public function getTitleFromUri($uri){
		$ret = $this->db->select('title')->from('page')->where('uri',$uri)->get()->row();
		return $ret->title; 
	}

}
