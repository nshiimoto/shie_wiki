<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class renderModel extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->pagePath = $this->config->item('page_path');
	}


	public function md($path){
		$parser = new \cebe\markdown\GithubMarkdown();
		$str = $parser->parse($this->_getContent($path));
		return array(
			'content' => $this->_getContentTitle($str),
			'title' =>$this->title,
			);
	}

	public function php($path){
		ob_start();
		eval('?>'.$this->_getContent($path));
		$str = ob_get_clean();
		return array(
			'content' => $this->_getContentTitle($str),
			'title' =>$this->title,
			);
	}


}
