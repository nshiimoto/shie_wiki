<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class settingModel extends MY_Model
{

	var $settingNameAry = array();
	public function __construct()
	{
		parent::__construct();
		$this->settingNameAry = $this->config->item('setting');
	}

	public function getIntervalTime(){
		// ->where('type',$this->settingNameAry['interval_time'])
		return $this->_getIntVal($this->settingNameAry['interval_time']);
	}

	public function getPageUnixTime(){
		return $this->_getIntVal($this->settingNameAry['page_time']);
	}
	public function getDirUnixTime(){
		return $this->_getIntVal($this->settingNameAry['dir_time']);
	}


	public function savePageUnixTime(){
		$ary =array(
			'type' => $this->settingNameAry['page_time'],
			'val' => time()
			);
		$this->db->replace('sys_setting', $ary);
	}

	public function saveDirUnixTime(){
		$ary =array(
			'type' => $this->settingNameAry['dir_time'],
			'val' => time()
			);
		$this->db->replace('sys_setting', $ary);
	}


	private function _getIntVal($type){
		$q = $this->db
		->select('val')
		->from('sys_setting')
		->where('type',$type)
		->get();
		$ret = $q->row();
		return intval($ret->val);
	}
}
