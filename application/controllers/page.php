<?php
/**
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class page extends MY_Controller {
	var $pagePath = '';
	var $data = array();
	var $layout = FALSE;

	public function __construct()
	{
		parent::__construct();
		$this->pagePath = $this->config->item('page_path');
		$this->load->model(array('PageModel','dirModel','settingModel'));
		$this->load->library(array('parser','render'));
	}

	public function index($pageUrl = 'index')
	{
		//ページ階層を取得
		$pageObj = $this->_boolContentFile($pageUrl);
		if($pageObj === FALSE){
			//無い。
			var_dump('404');
		}else{
			$content = $this->render->parse($pageObj->path,$this->data);
			$this->data = array_merge($this->data,$content);
		}
		$this->getSideHtml();
		$this->_render('page');
	}

	public function ajaxPageSave(){
		$interval = $this->settingModel->getIntervalTime();
		$pageLast = $this->settingModel->getPageUnixTime();
		if((time()-$pageLast) > $interval){
			$pageList = $this->pagelist->getFileListHush($this->pagePath);
			$this->PageModel->save($pageList,$this->data);
			$this->settingModel->savePageUnixTime();
			print 'update';
		}else{
			print 'short';
		}
	}
	public function ajaxDirSave(){
		$interval = $this->settingModel->getIntervalTime();
		$pageLast = $this->settingModel->getDirUnixTime();
		if((time()-$pageLast) > $interval){
			$pageList = $this->pagelist->getFileListHush($this->pagePath);
			$this->dirModel->save($pageList,$this->data);
			$this->settingModel->saveDirUnixTime();
			print 'update';
		}else{
			print 'short';
		}
	}




	/**
	 * アクセスされたURiがあるかをチェック
	 * 　1　DBに登録があれば、登録されているパスを返す。
	 * 　2　URIに/indexを追加してDBで確認
	 * 　3　実際にファイルがあれば、DBに登録して、パスを返す
	 * @param  [str]　リクエストURI　
	 * @return [str or FALSE]
	 */
	private function _boolContentFile($pageUrl){
		//DBへの登録チェック
		$path = $this->PageModel->getRow($pageUrl);
		if(!is_null($path)){
			return $path;
		}
		//indexをつけてDBへの登録チェック
		$path = $this->PageModel->getRow($pageUrl.'/index');
		if(!is_null($path)){
			return $path;
		}
		//最後に実際のファイルであるか？
		$pageList = $this->pagelist->getFileListHush($this->pagePath);
		foreach($pageList as $page){
			if(strpos($page,$pageUrl) === 0){
				//ファイルがあったので、DBに保存
				$ary = $this->PageModel->save(array($page),$this->data);
				//もう一回表示するのにリダイレクト
				redirect($this->data['rootUrl'].$pageUrl);
			}
		}
		//ないのでFALSEをかえす
		return FALSE;
	}


}
