<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MY_Controller extends CI_Controller {
	var $data = array();
	var $layout = 'layout';


	function __construct()
	{
		parent::__construct();
		$this->data = array_merge($this->data, $this->url->getUrlData());
	}


	/**
	 * 画面を表示する。
	 * viewsフォルダ内の
	 * @param  [str]  $view       viewsフォルダ内のviewファイルを指定する。
	 * @param  [str]  $layoutFile = NULL       レイアウトファイルの名を指定する場合は使用する。
	 * @return [type]              [description]
	 */
	public function _render($view,$layoutFile='layout')
	{
		$this->data['body'] = $this->parser->parse($view, $this->data,TRUE);
		if($this->layout !== FALSE){
			$view = $this->parser->parse("/element/".$layoutFile, $this->data,TRUE);
		}else{
			$view = $this->data['body'];
		}
		//ショートコード実行
		
		//画面表示
		print $view;
	}

	/**
	 * htmlライブラリに対してコンストラクタで行うメソッドをまとめる。
	 */
	private function _setHtmlLibrary(){

		//FeachライブラリにJSの情報を送る。
		$this->html->setJs($this->js_ori);
		$showTypeJsHash = "js_{$this->showType}";
		if(isset($this->$showTypeJsHash)){
			$this->html->setJs($this->$showTypeJsHash);
		}
		if(isset($this->js)){
			$this->html->setJs($this->js);
		}
		
		//FeachライブラリにCssの情報を送る。
		$this->html->setCss($this->css_ori);
		$showTypeCssHash = "css_{$this->showType}";
		if(isset($this->$showTypeCssHash)){
			$this->html->setCss($this->$showTypeCssHash);
		}
		if(isset($this->css)){
			$this->html->setCss($this->css);
		}
	}

	public function getSideHtml(){
		$top = $this->dirModel->getDirTree(0);
		unset($top[0]->child);
		$this->data['sideTree'] =array_merge($top, $this->dirModel->getDirTree(1));
	}

}
