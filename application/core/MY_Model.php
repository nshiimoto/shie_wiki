<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MY_Model extends CI_Model
{


	public function __construct()
	{
		parent::__construct();
	}

	protected function _getParentDir($dir){
		$ary = explode('/',$dir);
		$parent = '';
		for($i =0; $i < count($ary)-1; $i++){
			$parent .= $ary[$i].'/';
		}
		return ltrim($parent,'/');
	}
}
